<?php
/**
* 
*/
class DAOClientes
{
	private $conexao;

	function __construct()
	{

	}
    function Get(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from CLIENTES where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		$resultado = $stmt->fetch(PDO::FETCH_OBJ);
		
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $resultado;
	}

	function GetLista(){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from CLIENTES';
		$stmt = $con->prepare($sql);
		$stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}


	function Insert(string $NOME, string $CPF, datetime $DATANASCIMENTO, string $TELEFONE, string $WHATSAPP, string $CIDADE, string $BAIRRO, string $RUA, int $NUMERO , string $COMPLEMENTO, string $COMOFICOUSABENDO, string $PROFISSAO, string $LOCALTRABALHO, datetime $DATACADASTRO, string $OBSERVACAO)
	{

		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "insert into CLIENTES(NOME, CPF, DATANASCIMENTO, TELEFONE, WHATSAPP, CIDADE, BAIRRO, RUA, NUMERO, COMPLEMENTO, COMOFICOUSABENDO, PROFISSAO, LOCALTRABALHO, DATACADASTRO, OBSERVACAO) VALUES (:NOME, :CPF, :DATANASCIMENTO, :TELEFONE, :WHATSAPP, :CIDADE, :BAIRRO, :RUA, :NUMERO, :COMPLEMENTO, :COMOFICOUSABENDO, :PROFISSAO, :LOCALTRABALHO, :DATACADASTRO, :OBSERVACAO)";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':NOME', $NOME);
        $stmt->bindParam(':CPF', $CPF);
        $stmt->bindParam(':DATANASCIMENTO', $DATANASCIMENTO->format('Y-m-d H:i:s'));
        $stmt->bindParam(':TELEFONE', $TELEFONE);
        $stmt->bindParam(':WHATSAPP', $WHATSAPP);
        $stmt->bindParam(':CIDADE', $CIDADE);
        $stmt->bindParam(':BAIRRO', $BAIRRO);
        $stmt->bindParam(':RUA', $RUA);
        $stmt->bindParam(':NUMERO', $NUMERO);
        $stmt->bindParam(':COMPLEMENTO', $COMPLEMENTO);
        $stmt->bindParam(':COMOFICOUSABENDO', $COMOFICOUSABENDO);
        $stmt->bindParam(':PROFISSAO', $PROFISSAO);
        $stmt->bindParam(':LOCALTRABALHO', $LOCALTRABALHO);
        $stmt->bindParam(':DATACADASTRO', $DATACADASTRO->format('Y-m-d H:i:s'));
        $stmt->bindParam(':OBSERVACAO', $OBSERVACAO);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;

	}
                            
    function Delete(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'delete from CLIENTES where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            
    function Update(int $ID, string $NOME, string $CPF, datetime $DATANASCIMENTO, string $TELEFONE, string $WHATSAPP, string $CIDADE, string $BAIRRO, string $RUA, int $NUMERO, string $COMPLEMENTO, string $COMOFICOUSABENDO, string $PROFISSAO, string $LOCALTRABALHO, datetime $DATACADASTRO, string $OBSERVACAO){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "update CLIENTES set NOME = :NOME, CPF = :CPF, DATANASCIMENTO = :DATANASCIMENTO, TELEFONE = :TELEFONE, WHATSAPP = :WHATSAPP, CIDADE = :CIDADE, BAIRRO = :BAIRRO, RUA = :RUA, NUMERO = :NUMERO, COMPLEMENTO = :COMPLEMENTO, COMOFICOUSABENDO = :COMOFICOUSABENDO, PROFISSAO = :PROFISSAO, LOCALTRABALHO = :LOCALTRABALHO, DATACADASTRO = :DATACADASTRO , OBSERVACAO = :OBSERVACAO where ID = :id";
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->bindParam(':NOME', $NOME);
        $stmt->bindParam(':CPF', $CPF);
        $stmt->bindParam(':DATANASCIMENTO', $DATANASCIMENTO->format('Y-m-d H:i:s'));
        $stmt->bindParam(':TELEFONE', $TELEFONE);
        $stmt->bindParam(':WHATSAPP', $WHATSAPP);
        $stmt->bindParam(':CIDADE', $CIDADE);
        $stmt->bindParam(':BAIRRO', $BAIRRO);
        $stmt->bindParam(':RUA', $RUA);
        $stmt->bindParam(':NUMERO', $NUMERO);
        $stmt->bindParam(':COMPLEMENTO', $COMPLEMENTO);
        $stmt->bindParam(':COMOFICOUSABENDO', $COMOFICOUSABENDO);
        $stmt->bindParam(':PROFISSAO', $PROFISSAO);
        $stmt->bindParam(':LOCALTRABALHO', $LOCALTRABALHO);
        $stmt->bindParam(':DATACADASTRO', $DATACADASTRO->format('Y-m-d H:i:s'));
        $stmt->bindParam(':OBSERVACAO', $OBSERVACAO);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            



}
?>
