<?php
/**
* 
*/
class DAOAgendamento
{
	private $conexao;

	function __construct()
	{

	}
    function Get(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from agendamentos where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		$resultado = $stmt->fetch(PDO::FETCH_OBJ);
		
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $resultado;
	}

	function GetLista(){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from agendamentos';
		$stmt = $con->prepare($sql);
		$stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}

	function GetListaPorDia(datetime $DIA){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select a.*, c.NOME from AGENDAMENTOS a inner join CLIENTES c on c.ID = a.ID_CLIENTE where DIA = :DIA';
		$stmt = $con->prepare($sql);
		$DATA = $DIA->format('Y-m-d H:i:s');
		$stmt->bindParam(':DIA', $DATA);
		$stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}


	function GetAgendamentoDia(string $Hora, string $Profissional){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'SELECT NOME FROM CLIENTES WHERE ID IN( 
			    select ID_CLIENTE                    
			   from agendamentos                      
			   where ID_HORA = :xHORA                      
			    AND ID_PROFISSIONAL = :xPROFISSIONAL               
			    AND DIA = current_date())             ';

		$stmt = $con->prepare($sql);
		$stmt->bindParam(':xHORA', $Hora);
		$stmt->bindParam(':xPROFISSIONAL', $Profissional);
		$stmt->execute();
		$resultado = $stmt->fetch(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		return $resultado;
	}



	function Insert(datetime $DATA, int $idprofissional, int $idHorario, int $idCliente )
	{

		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "insert into agendamentos(DIA, ID_HORA, ID_PROFISSIONAL, ID_CLIENTE) VALUES (:DIA, :ID_HORA, :ID_PROFISSIONAL, :ID_CLIENTE)";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':DIA', $DATA->format('Y-m-d H:i:s'));
        $stmt->bindParam(':ID_HORA', $idHorario);
		$stmt->bindParam(':ID_PROFISSIONAL', $idprofissional);
		$stmt->bindParam(':ID_CLIENTE', $idCliente);
        $stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;

	}
                            
    function Delete(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'delete from agendamentos where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            
                            



}
?>
