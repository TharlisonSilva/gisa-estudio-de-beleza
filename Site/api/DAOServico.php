<?php
/**
* 
*/
class DAOServico
{
	private $conexao;

	function __construct()
	{

	}
    function GetServicos(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from servicos where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		$resultado = $stmt->fetch(PDO::FETCH_OBJ);
		
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $resultado;
	}

	function GetListaServicos(){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from servicos';
		$stmt = $con->prepare($sql);
		$stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}


	function InsertServicos(string $stDESCRICAO,
							string $stVALOR)
	{

		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "insert into SERVICOS(DESCRICAO, VALOR) VALUES (:descricao, REPLACE( REPLACE( :valor, '.' ,'' ), ',', '.' ))";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':descricao', $stDESCRICAO);
		$stmt->bindParam(':valor', $stVALOR);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;

	}
                            
    function DeleteServicos(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'delete from servicos where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            
    function UpdateServicos(int $ID,
                            string $stDESCRICAO,
		                    float $stVALOR){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "update servicos set DESCRICAO = :descricao, VALOR = REPLACE( REPLACE( :valor, '.' ,'' ), ',', '.' ) where ID = :id";
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->bindParam(':descricao', $stDESCRICAO);
		$stmt->bindParam(':valor', $stVALOR);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            



}
?>
