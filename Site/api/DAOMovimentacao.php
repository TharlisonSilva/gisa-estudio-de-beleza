<?php
/**
* 
*/
class DAOMovimentacao
{
	private $conexao;

	function __construct()
	{

	}
    function Get(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from servicos where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		$resultado = $stmt->fetch(PDO::FETCH_OBJ);
		
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $resultado;
	}

    function GetLista(){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select a.ID as ID, c.ID as IDCLIENTE , a.DATA as DATA, c.NOME as CLIENTE from MOVIMENTACOES a
				inner join clientes c on c.ID = a.ID_CLIENTE;';
		$stmt = $con->prepare($sql);
        $stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}


	function GetListaItemServicos(int $IdMovimentacao){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select A.ID AS ID, S.DESCRICAO AS SERVICO, P.NOME AS PROFISSIONAL, A.VALOR AS VALOR  from SERVICOXMOVIMENTACOES A 
				INNER JOIN servicos S ON S.ID = A.ID_SERVICO
				INNER JOIN profissionais P ON P.ID = A.ID_PROFISSIONAL 
				where ID_MOVIMENTACOES = :ID_MOVIMENTACOES';
		$stmt = $con->prepare($sql);
        $stmt->bindParam(':ID_MOVIMENTACOES', $IdMovimentacao);
        $stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}

    
	function Insert(datetime $dia, int $idCliente)
	{

		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "insert into MOVIMENTACOES(DATA, ID_CLIENTE) VALUES (:DATA, :ID_CLIENTE)";
		$stmt = $con->prepare($sql);
		$dia2 = $dia->format('Y-m-d H:i:s');
		$stmt->bindParam(':DATA', $dia2);
		$stmt->bindParam(':ID_CLIENTE', $idCliente);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		
		return $con->lastInsertId();

	}
                            
    function Delete(int $ID){
		
		$conexao = new conexao();
        $con = $conexao->connect();
        $sql = 'delete from SERVICOXMOVIMENTACOES where ID_MOVIMENTACOES = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		$sql = 'delete from MOVIMENTACOES where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}

	function InsertItemServicos(int $IdMovimentacao, int $IdProfissional, int $IdServico, float $valor)
	{

		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "insert into SERVICOXMOVIMENTACOES(ID_MOVIMENTACOES, ID_PROFISSIONAL, ID_SERVICO, VALOR) VALUES (:ID_MOVIMENTACOES, :ID_PROFISSIONAL, :ID_SERVICO, :VALOR)";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':ID_MOVIMENTACOES', $IdMovimentacao);
		$stmt->bindParam(':ID_PROFISSIONAL', $IdProfissional);
		$stmt->bindParam(':ID_SERVICO', $IdServico);
		$stmt->bindParam(':VALOR', $valor);
		$stmt->execute();
		echo ("a".$IdMovimentacao);
		echo ($IdProfissional);
		echo ($IdServico);
		echo ($valor);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;

	}
                            
    function DeleteItemServicos(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'delete from SERVICOXMOVIMENTACOES where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            
    
                            



}
?>
