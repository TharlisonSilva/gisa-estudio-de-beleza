<?php
/**
* 
*/
class DAOPagamentos
{
	private $conexao;

	function __construct()
	{

	}
    function Get(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from PAGAMENTOS where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		$resultado = $stmt->fetch(PDO::FETCH_OBJ);
		
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $resultado;
	}

    function GetPorComanda(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from PAGAMENTOS where ID_MOVIMENTACAO = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		$resultado = $stmt->fetch(PDO::FETCH_OBJ);
		
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $resultado;
	}

	function GetLista(){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from PAGAMENTOS';
		$stmt = $con->prepare($sql);
		$stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}

	function GetListaPorDia(datetime $DIA){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'SELECT a.*, c.NOME  
                    from PAGAMENTOS a
                    inner join MOVIMENTACOES B on B.ID = a.ID_MOVIMENTACAO AND B.DATA = :DIA
                    inner JOIN CLIENTES C ON C.ID = B.ID_CLIENTE;';
               
		$stmt = $con->prepare($sql);
		$DATA = $DIA->format('Y-m-d H:i:s');
        $stmt->bindParam(':DIA', $DATA);
		$stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}


	

	function Insert(datetime $TIPOPAGAMENTO, int $VALOR, int $DESCONTO, int $ID_MOVIMENTACAO )
	{

		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "insert into PAGAMENTOS(TIPOPAGAMENTO, VALOR, DESCONTO, ID_MOVIMENTACAO) VALUES (:TIPOPAGAMENTO, :VALOR, :DESCONTO, :ID_MOVIMENTACAO)";
		$stmt = $con->prepare($sql);
        $stmt->bindParam(':TIPOPAGAMENTO', $TIPOPAGAMENTO);
		$stmt->bindParam(':VALOR', $VALOR);
        $stmt->bindParam(':DESCONTO', $DESCONTO);
        $stmt->bindParam(':ID_MOVIMENTACAO', $ID_MOVIMENTACAO);
        $stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;

	}
                            
    function Delete(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'delete from PAGAMENTOS where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            
                            



}
?>
