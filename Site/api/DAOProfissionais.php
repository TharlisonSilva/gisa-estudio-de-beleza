<?php
/**
* 
*/
class DAOProfissionais
{
	private $conexao;

	function __construct()
	{

	}
    function Get(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from PROFISSIONAIS where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		$resultado = $stmt->fetch(PDO::FETCH_OBJ);
		
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $resultado;
	}

	function GetLista(){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'select * from PROFISSIONAIS';
		$stmt = $con->prepare($sql);
		$stmt->execute();
		$resultado = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$retorno = array();
		foreach ($resultado as $linha) {
		 	$retorno[] = $linha;
		}
		$con = null;
		return $retorno;
		
	}


	function Insert(string $NOME, string $TELEFONE)
	{

		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "insert into PROFISSIONAIS(NOME, TELEFONE) VALUES (:NOME, :TELEFONE)";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':NOME', $NOME);
        $stmt->bindParam(':TELEFONE', $TELEFONE);
        $stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;

	}
                            
    function Delete(int $ID){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'delete from PROFISSIONAIS where ID = :id';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            
    function Update(int $ID,
                            string $stNOME,
		                    float $stTELEFONE){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = "update PROFISSIONAIS set NOME = :NOME, TELEFONE = :TELEFONE where ID = :id";
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':id', $ID);
		$stmt->bindParam(':NOME', $stNOME);
		$stmt->bindParam(':TELEFONE', $stTELEFONE);
		$stmt->execute();
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $stmt->rowCount()>0;
	}
                            
	function GetHorarioDisponivel(datetime $data, int $idProfissional){
		
		$conexao = new conexao();
		$con = $conexao->connect();
		$sql = 'SELECT A.* FROM HORAS A LEFT JOIN agendamentos B ON B.ID_HORA = A.ID AND B.ID_PROFISSIONAL= :IDPROFISSIONAL AND B.DIA = :DATAD WHERE ISNULL(B.ID)';
        $stmt = $con->prepare($sql);
		$stmt->bindParam(':IDPROFISSIONAL', $idProfissional);
		$stringData = $data->format('Y-m-d');
		$stmt->bindParam(':DATAD', $stringData);
		$stmt->execute();
		$resultado = $stmt->fetchAll(PDO::FETCH_OBJ);
		
		if($stmt->errorCODE() != "00000")
		{
			$valido = false;
			$erro = "Erro código" .$stmt->errorCode().":";
			$erro.= implode($stmt->errorInfo());
			echo ($erro);
		}
		$con = null;
		return $resultado;
	}


}
?>
