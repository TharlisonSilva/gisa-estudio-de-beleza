<?php
include 'cabecalho.php';

$id = 0;
$NOME = "";
$TELEFONE = "";
$operacao = "";     
$dao = new DAOProfissionais();
$Profissionais = $dao->GetLista();

$daoCliente = new DAOClientes();
$Clientes = $daoCliente->GetLista();


if (isset($_REQUEST["Operacao"]))
    $operacao = $_REQUEST["Operacao"];
if (isset($_REQUEST["id"])){
    $id = $_REQUEST["id"];
    $dao = new DAOProfissionais();
    $Profissionais = $dao->GetLista();
}

?>
        <div id="CadastroAgendamento" class="contact section">
            <div class="container">
                <div class="row">
                    <div class="title fadeInDown">
                        <h2>Cadastro Agendamento</h2>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <form method="POST" action="controllAgendamentos.php/?Operacao=<?php echo($operacao)?>">
                            <div  class="contact-form">
                                <input type="hidden" name="id" value="<?php echo($id)?>">
                                
                                <div class="form-group">
                                    <input type="date" name="Data" id="Data" placeholder="Data" value="<?php echo date('Y-m-d');?>">
                                </div>
                                
                                <div class="form-group col-xs-2 col-sm-2 col-md-2">
                                    <select name="Profissional" class="form-control mr-sm-2 custom-select" id="exampleFormControlSelect1">
                                        <option value="" disabled selected>Profissional...</option>
                                        <?php
                                            foreach ($Profissionais as $profissional) {
                                                echo '<option name="Profissional" value="'.$profissional["ID"].'">'.$profissional["NOME"].'</option>';
                                            }
                                        ?>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-xs-2 col-sm-2 col-md-2">
                                    <select name="Cliente" class="form-control mr-sm-2 custom-select" id="exampleFormControlSelect1">
                                        <option value="" disabled selected>Cliente...</option>
                                        <?php
                                            foreach ($Clientes as $cliente) {
                                                echo '<option name="Cliente" value="'.$cliente["ID"].'">'.$cliente["NOME"].'</option>';
                                            }
                                        ?>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-xs-3 col-sm-3 col-md-3">
                                    <select name='horarios' class="form-control custom-select" id="exampleFormControlSelect1">
                                        <option value="" disabled selected>Horário...</option>
                                    </select>
                                </div>
                                <div class="form-group col-xs-3 col-sm-3 col-md-3">
                                    <button type="submit"  class="btn btn-theme-color">Salvar</button>
                            
                                </div>
                            </div>
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
<?php
include 'rodape.php';
?>

<script>
var data = $("#Data").val();

$("#Data").blur(function() {
    data = $("#Data").val();
       
    
});    


    $('select[name=Profissional]').blur(function(){
        $('select[name=horarios]').empty();
        var id = $('select[name=Profissional]').val();
        $.ajax({ // ajax
            type: "POST",
            url: "ControllAgendamentos.php",
            data: { idProfissional : id, data : data }, 
            success: function(result) {
                console.log(data);  
                console.log(id);
                result = JSON.parse(result);
                
                console.log(result);    
            
            if(true) {
                for (var i = 0; i < result.length; i++) {
                    $('select[name=horarios]').append('<option name="Hora" value="' + result[i].ID + '">' + result[i].HORA + "</option>");
                }
            } else {
                $('p').text('nao encontrado');
            }
        } 
    });
});

</script>