<?php
    include 'cabecalho.php';
    $dao = new DAOProfissionais();
    $profissionais = $dao-> GetLista();
  
?>
<div id="CadastroServico" class="contact section">
  <div class="container">
    <div class="row">
    <div class="title fadeInDown">
        <h2>PROFISSIONAIS</h2>
    </div>
    <a href="profissionais.php"><button type="button" id="Novo" class="btn btn-theme-color">Novo</button></a>
      <table class="table table-hover">
        <thead>
            <tr>
              <th scope="col">Ação</th>
              <th scope="col">Nome</th>
              <th scope="col">Telefone</th>
            </tr>
          </thead>
          <tbody>
          <?php
          foreach ($profissionais as $profissional) {
            echo '<tr>';
              echo '<td scope="row">
                <a class="btn btn-warning btn-xs" href="Profissionais.php?id='.$profissional["ID"].'">Editar</a>
                <a class="btn btn-danger btn-xs"  href="ControllProfissionais.php?id='.$profissional["ID"].'&Operacao=delete">Excluir</a></th>';
              echo '<td>'.$profissional["NOME"].'</td>';
              echo '<td>'.$profissional["TELEFONE"].'</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>



<?php
    include 'rodape.php';
?>