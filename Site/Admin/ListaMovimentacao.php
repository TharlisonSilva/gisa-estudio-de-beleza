<?php
    include 'cabecalho.php';
    $dao = new DAOMovimentacao();
    $movimentacoes = $dao-> GetLista();
  
?>
<div id="ListaMovimentacao" class="contact section">
  <div class="container">
    <div class="row">
    <div class="title fadeInDown">
        <h2>Comandas</h2>
    </div>
    <a href="movimentacao.php"><button type="button" id="Novo" class="btn btn-theme-color">Novo</button></a>
      <table class="table table-hover">
        <thead>
            <tr>
              <th scope="col">Ação</th>
              <th scope="col">Data</th>
              <th scope="col">Cliente</th>
            </tr>
          </thead>
          <tbody>
          <?php
          foreach ($movimentacoes as $movimento) {
            echo '<tr>';
              echo '<td scope="row">
                <a class="btn btn-warning btn-xs" href="movimentacao.php?id='.$movimento["ID"].'&idCli='.$movimento["IDCLIENTE"].'">Editar</a>
                <a class="btn btn-danger btn-xs"  href="controllMovimentacao.php?idM='.$movimento["ID"].'&Operacao=delete">Excluir</a></th>';
              echo '<td>'.(new DateTime( $movimento["DATA"]))-> format( "d/m/Y" ).'</td>';
              echo '<td>'.$movimento["CLIENTE"].'</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>



<?php
    include 'rodape.php';
?>