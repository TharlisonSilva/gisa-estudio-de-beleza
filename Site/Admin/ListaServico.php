<?php
    include 'cabecalho.php';
    $daoServico = new DAOServico();
    $servicos = $daoServico-> GetListaServicos();
  
?>
<div id="CadastroServico" class="contact section">
  <div class="container">
    <div class="row">
    <div class="title fadeInDown">
        <h2>Serviços</h2>
    </div>
    <a href="Servico.php"><button type="button" id="Novo" class="btn btn-theme-color">Novo</button></a>
      <table class="table table-hover">
        <thead>
            <tr>
              <th scope="col">Ação</th>
              <th scope="col">Descrição</th>
              <th scope="col">Valor</th>
            </tr>
          </thead>
          <tbody>
          <?php
          foreach ($servicos as $servico) {
            echo '<tr>';
              echo '<td scope="row">
                <a class="btn btn-warning btn-xs" href="Servico.php?id='.$servico["ID"].'">Editar</a>
                <a class="btn btn-danger btn-xs"  href="ControllServico.php?id='.$servico["ID"].'&Operacao=delete">Excluir</a></th>';
              echo '<td>'.$servico["DESCRICAO"].'</td>';
              echo '<td>'.$servico["VALOR"].'</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>



<?php
    include 'rodape.php';
?>