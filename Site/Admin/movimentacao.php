<?php
include 'cabecalho.php';

$id = 0;
$IdCli = 0;
$operacao = 0;


$daoCliente = new DAOClientes();
$Clientes = $daoCliente->GetLista();


$daoServico = new DAOServico();
$Servicos = $daoServico->GetListaServicos();


$daoProfissionais = new DAOProfissionais();
$Profissionais = $daoProfissionais->GetLista();


$daoMovimentacao = new DAOMovimentacao();


if (isset($_REQUEST["Operacao"]))
    $operacao = $_REQUEST["Operacao"];
if (isset($_REQUEST["id"])){
    $id = $_REQUEST["id"]; 
    $IdCli = $_REQUEST["idCli"];
    $movimentacaoListaItem = $daoMovimentacao->GetListaItemServicos($id);
}

?>
        <div id="CadastroMovimentacao" class="contact section">
            <div class="container">
                <div class="row">
                    <div class="title fadeInDown">
                        <h2>Comanda de Serviço</h2>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <form method="POST" id="FormidM" action="ControllMovimentacao.php/?Operacao=<?php echo($operacao)?>">
                            <div  class="contact-form">
                                <input type="hidden" id="idM" name="idM" value="<?php echo($id)?>">
                                <input type="hidden" id="e" name="e" value="">
                                <div class="form-group">
                                    <input type="date" name="Data" id="Data" placeholder="Data" value="<?php echo date('Y-m-d');?>">
                                </div>
                                <div class="form-group col-xs-2 col-sm-2 col-md-2">
                                    <select name="Cliente" class="form-control mr-sm-2 custom-select" id="Cliente">
                                        <option value="" disabled selected>Cliente...</option>
                                        <?php
                                            foreach ($Clientes as $cliente) {
                                                if ($cliente["ID"]==$IdCli)
                                                    echo '<option name="Cliente" selected value="'.$cliente["ID"].'">'.$cliente["NOME"].'</option>';
                                                else
                                                    echo '<option name="Cliente" value="'.$cliente["ID"].'">'.$cliente["NOME"].'</option>';
                                                
                                            }
                                        ?>
                                        
                                    </select>
                                </div>
                                <div class="form-group col-xs-3 col-sm-3 col-md-3">
                                    <button type="submit" id="submitIdM" class="btn btn-theme-color">Salvar</button>
                            
                                </div>
                            </div>
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
        <?php
            if ($id != 0){
                
        ?>
         <div id="CadastroItemServico" class="">
            <div class="container">
                <div class="row">
                <div class="title fadeInDown">
                        <h2>Incluir Serviço</h2>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                                <form method="POST" action="ControllMovimentacao.php/?Operacao=InsertItem">
                                    <div  class="contact-form">
                                        <input type="hidden" name="idM" value="<?php echo($id)?>">
                                        <input type="hidden" id="IdCli" name="IdCli" value="<?php echo($IdCli)?>">
                                        <div class="form-group col-xs-2 col-sm-2 col-md-2">
                                            <select name="Profissionais" class="form-control mr-sm-2 custom-select" id="exampleFormControlSelect1">
                                                <option value="" disabled selected>Profissionais...</option>
                                                <?php
                                                    foreach ($Profissionais as $Profissional) {
                                                        echo '<option name="Profissional" value="'.$Profissional["ID"].'">'.$Profissional["NOME"].'</option>';
                                                    }
                                                ?>
                                                
                                            </select>
                                        </div>
                                        <div class="form-group col-xs-2 col-sm-2 col-md-2">
                                            <select name="Servicos" class="form-control mr-sm-2 custom-select" id="exampleFormControlSelect1">
                                                <option value="" disabled selected>Serviços...</option>
                                                <?php
                                                    foreach ($Servicos as $Servico) {
                                                        echo '<option name="Servico" value="'.$Servico["ID"].'">'.$Servico["DESCRICAO"].'</option>';
                                                    }
                                                ?>
                                                
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="text" name="Valor" id="Valor" placeholder="Valor" value="<?php ?>">
                                        </div>
                                        <div class="form-group col-xs-3 col-sm-3 col-md-3">
                                            <button type="submit"  class="btn btn-theme-color">Salvar</button>
                                    
                                        </div>
                                    </div>
                                </form>
                            </div>
                        
                </div>
            </div>
        </div>



        <div id="CadastroServico" class="">
  <div class="container">
    <div class="row">
      <table class="table table-hover">
        <thead>
            <tr>
              <th scope="col">Ação</th>
              <th scope="col">Profissional</th>
              <th scope="col">Serviço</th>
              <th scope="col">Valor</th>
            </tr>
          </thead>
          <tbody>
          <?php
          foreach ($movimentacaoListaItem as $item) {
            echo '<tr>';
              echo '<td scope="row">
                <a class="btn btn-danger btn-xs"  href="ControllMovimentacao.php?idM='.$id.'&IdCli='.$IdCli.'&idIM='.$item["ID"].'&Operacao=DeleteItem">Excluir</a></th>';
              echo '<td>'.$item["PROFISSIONAL"].'</td>';
              echo '<td>'.$item["SERVICO"].'</td>';
              echo '<td>'.$item["VALOR"].'</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table>
        <!-- Botão para acionar modal -->
<button type="button" class="btn btn-theme-color" data-toggle="modal" data-target="#modalExemplo">
  Encerrar comanda
</button>
      </div>
    </div>
  </div>


<!-- Modal -->
<div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
            include 'pagamento.php';
        ?>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-theme-color">Salvar</button>
      </div>
    </div>
  </div>
</div>

        <?php

            }
        ?>




<?php
include 'rodape.php';
?>

<script>
if ($("#idM").val() != 0){
    $('#FormidM').attr('disabled','true');
    $('#Data').attr('disabled','true');
    $('#Cliente').attr('disabled','true');
    $('#submitIdM').attr('disabled','true');
    $('#e').val(99);
    
}


$('select[name=Servicos]').blur(function(){
        
        var id = $('select[name=Servicos]').val();
        $.ajax({ // ajax
            type: "POST",
            url: "ControllMovimentacao.php",
            data: { idServico : id }, 
            success: function(result) {
                console.log(result);  
                result = JSON.parse(result);
                
                console.log(result);    
                $("#Valor").val(result.VALOR);
            
        } 
    });
});

</script>