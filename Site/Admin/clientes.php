<?php
include 'cabecalho.php';
$id = 0;
$NOME = "";
$CPF = "";
$DATANASCIMENTO = "";
$TELEFONE = "";
$WHATSAPP = "";
$CIDADE = "";
$BAIRRO = "";
$RUA = "";
$NUMERO = 0;
$COMPLEMENTO = "";
$COMOFICOUSABENDO = "";
$PROFISSAO = "";
$LOCALTRABALHO = "";
$OBSERVACAO = "";
$operacao = "";


if (isset($_REQUEST["Operacao"]))
    $operacao = $_REQUEST["Operacao"];
if (isset($_REQUEST["id"])){
    $id = $_REQUEST["id"];
    $daoClientes = new DAOClientes();
    $cliente = $daoClientes->Get($id);
    $NOME = $cliente->NOME;
    $CPF = $cliente->CPF;
    $DATANASCIMENTO = $cliente->DATANASCIMENTO;
    $TELEFONE = $cliente->TELEFONE;
    $WHATSAPP = $cliente->WHATSAPP;
    $CIDADE = $cliente->CIDADE;
    $BAIRRO = $cliente->BAIRRO;
    $RUA = $cliente->RUA;
    $NUMERO = $cliente->NUMERO;
    $COMPLEMENTO = $cliente->COMPLEMENTO;
    $COMOFICOUSABENDO = $cliente->COMOFICOUSABENDO;
    $PROFISSAO = $cliente->PROFISSAO;
    $LOCALTRABALHO = $cliente->LOCALTRABALHO;
    $OBSERVACAO = $cliente->OBSERVACAO;

}

?>
        <div id="CadastroServico" class="contact section">
            <div class="container">
                <div class="row">
                    <div class="title fadeInDown">
                        <h2>Cadastro Clientes</h2>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <form method="POST" action="ControllClientes.php/?Operacao=<?php echo($operacao)?>"> 
                            <div  class="contact-form">
                                <input type="hidden" name="id" value='<?php echo($id)?>'>
                                <input type="text" name="NOME" id="NOME" placeholder="Nome" value="<?= $NOME ?>">
                                <input type="text" name="CPF" id="CPF" placeholder="CPF" value="<?= $CPF ?>">
                                <input type="date" name="DATANASCIMENTO" id="DATANASCIMENTO" placeholder="Data de nascimento" value="<?= $DATANASCIMENTO ?>">
                                <input type="text" name="TELEFONE" id="TELEFONE" placeholder="Telefone" value="<?= $TELEFONE ?>">
                                <input type="text" name="WHATSAPP" id="WHATSAPP" placeholder="Whatsapp" value="<?= $WHATSAPP ?>">
                                <input type="text" name="CIDADE" id="CIDADE" placeholder="Cidade" value="<?= $CIDADE ?>">
                                <input type="text" name="BAIRRO" id="BAIRRO" placeholder="Bairro" value="<?= $BAIRRO ?>">
                                <input type="text" name="RUA" id="RUA" placeholder="Rua" value="<?= $RUA ?>">
                                <input type="text" name="NUMERO" id="NUMERO" placeholder="Número" value="<?= $NUMERO ?>">
                                <input type="text" name="COMPLEMENTO" id="COMPLEMENTO" placeholder="Complemento" value="<?= $COMPLEMENTO ?>">
                                <input type="text" name="COMOFICOUSABENDO" id="COMOFICOUSABENDO" placeholder="Como ficou sabendo?" value="<?= $COMOFICOUSABENDO ?>">
                                <input type="text" name="PROFISSAO" id="PROFISSAO" placeholder="Profissão" value="<?= $PROFISSAO ?>">
                                <input type="text" name="LOCALTRABALHO" id="LOCALTRABALHO" placeholder="Local de trabalho" value="<?= $LOCALTRABALHO ?>">
                                <textarea name="OBSERVACAO" id="OBSERVACAO" cols="30" rows="10" class="input-message" placeholder="Observação"><?= $OBSERVACAO ?></textarea>
                                <div class="col-xs-12 col-sm-8 col-md-8"> 
                                    <button type="submit"  class="btn btn-theme-color">Salvar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                   
                </div>
            </div>
        </div>
<?php
include 'rodape.php';
?>

<script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>     
<script>
$('#TELEFONE').mask('(00) 0 0000-0000');
$('#WHATSAPP').mask('(00) 0 0000-0000');
$('#CPF').mask('000.000.000-00');


</script>