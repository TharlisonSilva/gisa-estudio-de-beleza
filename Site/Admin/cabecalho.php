<?php
  session_start();
  if( ($_SESSION["id_usuario"] == null) || ($_SESSION["id_usuario"] == 0) ){
    header("Location: ../Cliente/");
    exit;
  }
  if(($_SESSION["nome_usuario"] == null ) || ($_SESSION["nome_usuario"] == 'Visitante')){
    header("Location: ../Cliente/");
    exit;
  }
  require "../api/api.php";
?>
<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Author Meta -->
        <meta name="author" content="Themefisher">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="utf-8">

         <!-- Site Title -->
         <title>Gisa Studio Beleza</title>


        <!-- Favicon -->
        <link rel="shortcut icon" href="img/logo.png">

        <link href='https://fonts.googleapis.com/css?family=Raleway:200,300,400,700,900,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:200,300,400,900,700,500,300' rel='stylesheet' type='text/css'>


        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/justifiedGallery.min.css">
        <link rel="stylesheet" href="css/et-font.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/style2.css">

        <script src="js/modernizr-custom.js"></script>

    </head>

    <body>
        <!-- #preloader -->
        <div id="preloader">
            <div class="preloader loading">
                <span class="slice"></span>
                <span class="slice"></span>
                <span class="slice"></span>
                <span class="slice"></span>
                <span class="slice"></span>
                <span class="slice"></span>
            </div>
        </div>
        <!-- end #preloader -->

        <header>

            <nav class="navbar navbar-fixed-top" role="navigation">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="index.html">
                                    <img src="img/logo.png" height="45" width="104" alt="">
                                </a>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse navbar-ex1-collapse">


                              <ul class="nav navbar-nav navbar-right main-menu">
                                  <li><a href="#">Inicio</a></li>
                                  <li><a href="agendamento.php">Agendar</a></li>
                                    <li><a href="ListaMovimentacao.php">Comandas</a></li>
                                  <li id="Admin_Cadastros_Cabecalho" ><a href="#" class="menu-nav"> Cadastros</a>
                                    <ul id="Admin_Cadastros">
                                      <li><a href="ListaClientes.php"> Clientes </a></li>
                                      <li><a href="ListaProfissionais.php"> Profissionais </a></li>
                                      <li><a href="ListaServico.php"> Serviços </a></li>
                                    </ul>
                                  </li>
                                  <li><a href="Login/ValidarAcesso.php"> Sair </a></li>
                              </ul>

                            </div><!-- /.navbar-collapse -->
                        </div>
                    </div>
                </div>
            </nav>
                  <!-- Conteudo -->
