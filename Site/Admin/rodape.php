 <!-- // Conteudo -->



        </header>

        <footer class="footer">
            <div class="conainer text-center wow zoomIn">
                <div class="social-icons clearfix">
                    <a class="facebook" href="http://www.fb.com/themefisher">
                        <i class="icon-facebook"></i>
                    </a>

                    <a class="twitter" href="http://www.twitter.com/themefisher">
                        <i class="icon-twitter"></i>
                    </a>

                    <a class="dribbble" href="http://www.themefisher.com/themefisher">
                        <i class="icon-dribbble"></i>
                    </a>

                    <a class="linkedin" href="http://www.linkedin.com/themefisher">
                        <i class="icon-linkedin"></i>
                    </a>

                    <a class="googleplus" href="http://www.plus.google.com/themefisher">
                        <i class="icon-googleplus"></i>
                    </a>


                </div>

                <div class="copyright">
                    <p>Copyright:<a href="http://www.themefisher.com">Themefisher</a>, All Rights Reserved.</p>
                </div>
            </div>
        </footer>


        <!--
        JavaScripts
        ========================== -->


        <script src="js/vendor/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.justifiedGallery.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/main.js"></script>

        <!-- gallery -->
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/masonry.pkgd.min.js"></script>
        <script src="js/classie.js"></script>
        <script src="js/grid.js"></script>
    </body>
</html>
