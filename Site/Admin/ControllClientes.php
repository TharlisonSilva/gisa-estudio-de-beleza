<?php
require "../api/api.php";
$daoClientes = new DAOClientes();

if (isset($_POST["id"])){
    $id = $_POST["id"];
    $NOME = $_POST["NOME"];
    $CPF = $_POST["CPF"];
    $DATANASCIMENTO = $_POST["DATANASCIMENTO"];
    $TELEFONE = $_POST["TELEFONE"];
    $WHATSAPP = $_POST["WHATSAPP"];
    $CIDADE = $_POST["CIDADE"];
    $BAIRRO = $_POST["BAIRRO"];
    $RUA = $_POST["RUA"];
    $NUMERO = $_POST["NUMERO"];
    $COMPLEMENTO = $_POST["COMPLEMENTO"];
    $COMOFICOUSABENDO = $_POST["COMOFICOUSABENDO"];
    $PROFISSAO = $_POST["PROFISSAO"];
    $LOCALTRABALHO = $_POST["LOCALTRABALHO"];
    $DATACADASTRO = date("Y/m/d");
    $OBSERVACAO = $_POST["OBSERVACAO"];
}else{
    $id = $_REQUEST["id"];
}


if ($id==0){//insert
    $daoClientes->Insert($NOME,$CPF,new DateTime($DATANASCIMENTO),$TELEFONE,$WHATSAPP,$CIDADE,$BAIRRO,$RUA,$NUMERO,$COMPLEMENTO,$COMOFICOUSABENDO,$PROFISSAO,$LOCALTRABALHO,new DateTime($DATACADASTRO),$OBSERVACAO);
    header('Location: ../ListaClientes.php');
}else if (isset($_REQUEST["Operacao"]) && $_REQUEST["Operacao"] == "delete" && $id != 0){//delete
    $daoClientes->Delete($id);
    header('Location: ListaClientes.php');
}else if (id != 0){//update
    $daoClientes->Update($id,$NOME,$CPF,new DateTime($DATANASCIMENTO),$TELEFONE,$WHATSAPP,$CIDADE,$BAIRRO,$RUA,$NUMERO,$COMPLEMENTO,$COMOFICOUSABENDO,$PROFISSAO,$LOCALTRABALHO,new DateTime($DATACADASTRO),$OBSERVACAO);
}
header('Location: ../ListaClientes.php');
?>