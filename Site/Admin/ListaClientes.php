<?php
    include 'cabecalho.php';
    $daoCliente = new DAOClientes();
    $clientes = $daoCliente-> GetLista();
  
?>
<div id="CadastroServico" class="contact section">
  <div class="container">
    <div class="row">
    <div class="title fadeInDown">
        <h2>Clientes</h2>
    </div>
    <a href="clientes.php"><button type="button" id="Novo" class="btn btn-theme-color">Novo</button></a>
      <table class="table table-hover">
        <thead>
            <tr>
              <th scope="col">Ação</th>
              <th scope="col">Nome</th>
              <th scope="col">Telefone</th>
              <th scope="col">WhastApp</th>
            </tr>
          </thead>
          <tbody>
          <?php
          foreach ($clientes as $cliente) {
            echo '<tr>';
              echo '<td scope="row">
                <a class="btn btn-warning btn-xs" href="clientes.php?id='.$cliente["ID"].'">Editar</a>
                <a class="btn btn-danger btn-xs"  href="ControllClientes.php?id='.$cliente["ID"].'&Operacao=delete">Excluir</a></th>';
              echo '<td>'.$cliente["NOME"].'</td>';
              echo '<td>'.$cliente["TELEFONE"].'</td>';
              echo '<td>'.$cliente["WHATSAPP"].'</td>';
            echo '</tr>';
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>



<?php
    include 'rodape.php';
?>