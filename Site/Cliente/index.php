<?php   session_start();
        if( ($_SESSION["id_usuario"] == null) || ($_SESSION["nome_usuario"] == null)){
            $_SESSION["id_usuario"]   = 0;
            $_SESSION["nome_usuario"] = 'Visitante';
        }

?>
<!DOCTYPE html>
<html lang="pt-br" class="no-js">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Author Meta -->
        <meta name="author" content="Themefisher">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="utf-8">

        <!-- Site Title -->
        <title>Gisa Studio Beleza</title>


        <!-- Favicon -->
        <link rel="shortcut icon" href="img/icons/">

        <link href='https://fonts.googleapis.com/css?family=Raleway:200,300,400,700,900,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:200,300,400,900,700,500,300' rel='stylesheet' type='text/css'>


        <!--
        CSS
        ============================================= -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/justifiedGallery.min.css">
        <link rel="stylesheet" href="css/et-font.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/main.css">

    </head>

    <body id="Inicio">
        <div id="preloader">
            <div class="preloader loading">
                <span class="slice"></span>
                <span class="slice"></span>
                <span class="slice"></span>
                <span class="slice"></span>
                <span class="slice"></span>
                <span class="slice"></span>
            </div>
        </div>
        <!-- end #preloader -->
        <header class="site-header navbar-fixed-top navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>

                <div class="nav-toggle hidden-xs">
                    <button class="toggle-btn">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <nav class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav main-manu">
                        <li><a href="#Inicio">Inicio</a></li>
                        <li><a href="#about">Sobre Nós</a></li>
                        <li><a href="#process">Serviço</a></li>
                        <li><a href="#contact">Contato</a></li>
                        <li><a href="../Admin/Login/">Entrar</a></li>
                    </ul>
                </nav><!-- /.navbar-collapse -->
            </div>
        </header>

        <div class="home-banner fullscreen" >
            <div class="gradient"></div>
            <div class="banner-content dtable fullscreen">
                <div class="content-inner dtablecell">
                    <div class="container">
                        <h1>Gisa Studio Beleza</h1>
                    </div>
                </div>
            </div>
        </div>

        <!-- about -->
        <div id="about" class="about">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5 wow fadeInLeft">
                        <div class="author-thumb text-center">
                            <img src="img/author.png" alt="">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-7 wow fadeInRight">
                        <div class="about-intro">
                            <h3>Gisa Studio Beleza</h3>
                            <h5>Atendimento personalizado. Profissionais com cursos e constante aperfeiçoamento proporcionam um ótimo resultado nos cuidados de sua aparência. Cortes, tinturas, reflexos, luzes, e todo que um completo salão de beleza pode oferecer.</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // about -->

        <!-- process -->
        <div id="process" class="process section">
            <div class="container">
                <div class="row">
                    <div class="title">
                        <h2>Meu Trabalho</h2>
                        <p>A minha melhor obra de Arte é ver o sorriso no rosto de cada Cliente!</p>
                    </div>

                    <div class="proecess-block col-xs-10 col-sm-5 col-md-3">
                        <div class="process-inner">
                            <div class="icon-holder">
                                <img src="img/estetica.png" alt="">
                            </div>
                            <h4 class="heading">Estética</h4>
                            <p class="description">Limpeza de pele<br>Drenagem Linfática<br>Massagem Modeladora<br>Massagem Relaxante...</p>
                        </div>
                    </div>
                    <div class="proecess-block col-xs-10 col-sm-5 col-md-3">
                        <div class="process-inner">
                            <div class="icon-holder">
                                <img src="img/esmalteria.png" alt="">
                            </div>
                            <h4 class="heading">Esmalteria</h4>
                            <p class="description">Decoração de unha<br>Unha Postiça<br>Mão e Pé<br>Pacotes de unhas Mensal...</p>
                        </div>
                    </div>

                    <div class="proecess-block col-xs-10 col-sm-5 col-md-3">
                        <div class="process-inner">
                            <div class="icon-holder">
                                <img src="img/mascara.png" alt="">
                            </div>
                            <h4 class="heading">Make</h4>
                            <p class="description">Design de Sobrancelha<br>Design com Henna<br>Micropigmentação<br>Maquiagem Social...</p>
                        </div>
                    </div>

                    <div class="proecess-block col-xs-10 col-sm-5 col-md-3 ">
                        <div class="process-inner">
                            <div class="icon-holder">
                                 <img src="img/studio.png" alt="">
                            </div>
                            <h4 class="heading">Studio</h4>
                            <p class="description">Progressiva<br>Penteados<br>Hidratação<br>Reconstrução...</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- // process -->



        <!-- works -->
        <div id="works" class="works section">
            <div class="gradient"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title fadeInDown">
                            <h2>Exposição</h2>
                        </div>

                        <div class="works-gallery">
                            <div id="mygallery" >
                                <div>
                                    <img alt="" src="img/work/loiros.jpg"/>
                                    <div class="item-musk">
                                        <div class="item-caption">
                                            <h4>Loiros</h4>
                                        </div>
                                    </div>

                                </div>
                                <div>
                                    <img alt="" src="img/work/loirs.jpg"/>
                                    <div class="item-musk">
                                        <div class="item-caption">
                                            <h4>Loiros Platinados</h4>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <img alt="" src="img/work/unhas.jpg"/>
                                    <div class="item-musk">
                                        <div class="item-caption">
                                            <h4>Unhas decoradas</h4>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <img alt="" src="img/work/unha.jpg"/>
                                    <div class="item-musk">
                                        <div class="item-caption">
                                            <h4>Unha Simples.</h4>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <img alt="" src="img/work/make.jpg"/>
                                    <div class="item-musk">
                                        <div class="item-caption">
                                            <h4>Maquiagens</h4>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <img alt="" src="img/work/penteados.jpg"/>
                                    <div class="item-musk">
                                        <div class="item-caption">
                                            <h4>Penteados</h4>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <img alt="" src="img/work/limpeza.jpg"/>
                                    <div class="item-musk">
                                        <div class="item-caption">
                                            <h4>Estética</h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- other images... -->
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- // works -->

        <!-- contact -->
        <div id="contact" class="contact section">
            <div class="container">
                <div class="row">
                    <div class="title fadeInDown">
                        <h2>Contato</h2>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <!-- <div class="exp-block">
                            <h3>Leave a message</h3>
                            <form action="#" class="contact-form clearfix">
                                <div class="input-field">
                                    <input type="text" name="name" placeholder="Name*" class="form-control">
                                </div>
                                <div class="input-field">
                                    <input type="email" name="email" placeholder="Email*" class="form-control">
                                </div>
                                <div class="input-field">
                                    <textarea name="message" placeholder="Message*" class="form-control"></textarea>
                                </div>
                                <button type="submit" class="btn btn-theme-color pull-right">Send Message</button>
                            </form>
                        </div> -->
                        <div  class="contact-form">
                          <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d3557.1519309813634!2d-48.965797496538045!3d-26.93039754678738!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x94df23592e230357%3A0xd08b69b9ab2fb86c!2sR.%20Pref.%20Leopoldo%20Schramm%2C%20150%20-%20Coloninha%2C%20Gaspar%20-%20SC%2C%2089114-442!3m2!1d-26.9300006!2d-48.9637161!5e0!3m2!1spt-BR!2sbr!4v1573605528765!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <div class="contact-address">
                            <h4>Endereço</h4>
                            <ul>
                                <li class="address">R. Pref. Leopoldo Schramm, 150<br>
                                Coloninha - 89114-442<br>Gaspar-SC</li>

                                <li class="phone">(47) 3332-9360</li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- // contact -->

        <footer class="footer">
            <div class="conainer text-center wow zoomIn">
                <div class="social-icons clearfix">
                    <a class="facebook" href="https://www.facebook.com/gisahairblond">
                        <i class="icon-facebook"></i>
                    </a>

                    <a class="linkedin" href="">
                        <i class="icon-linkedin"></i>
                    </a>

                    <a class="googleplus" href="">
                        <i class="icon-googleplus"></i>
                    </a>


                </div>

                <div class="copyright">
                    <p>Copyright</a> All Rights Reserved.</p>
                </div>
            </div>
        </footer>


        <!--
        JavaScripts
        ========================== -->



        <script src="js/vendor/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.justifiedGallery.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
